# -*- coding: utf-8 -*-
"""
Created on Tue May 10 18:42:40 2016

brownian.py

@author: Edwin
"""

from scipy.stats import norm
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

def brownian(x0, n, dt, delta, out=None):
    x0 = np.asarray(x0)
    r = norm.rvs(size=x0.shape + (n,), scale=delta*np.sqrt(dt))
    if out is None:
        out = np.empty(r.shape)
    
    np.cumsum(r, axis=1, out=out)
    out += np.expand_dims(x0, axis=-1)
    
    return out

def main():
    
    delta = 0.25
    T = 10.0
    N = 1000
    dt = T/N
    x = np.empty((3,N+1))
    x[:, 0] = np.zeros(3)
    
    for _ in xrange(1000):
        brownian(x[:, 0], N, dt, delta, out=x[:,1:])
    
    #plot in 2D
    plt.plot(x[0], x[1])
    plt.title('Movimiento browniano 2D')
    plt.axis('equal')
    plt.grid(True)
    plt.show()  
    
    #plot in 3D
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.plot(x[0], x[1],x[2])
    plt.title('Movimiento browniano en 3D')
    
    
if __name__ == "__main__":
    main()