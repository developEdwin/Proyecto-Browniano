# -*- coding: utf-8 -*-
"""
Created on Wed May 11 17:48:38 2016

@author: Edwin
"""

import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

def rayleigh(x0, n, dt, delta):

    x0 = np.asfarray(x0)
    shp = (n+1,) + x0.shape

    r = np.random.normal(size=shp, scale=delta*np.sqrt(dt))

    r[0] = 0.0

    x = r.cumsum(axis=0)
    x += x0

    return x

xinicial = np.zeros(2)  # número de partículas
n = 1000
dt = 10.0
delta = 0.25
xini = np.array(rayleigh(xinicial, n, dt, delta))
yini = np.array(rayleigh(xinicial, n, dt, 0.25))
zini = np.array(rayleigh(xinicial, n, dt, 0.2))

# *--- Plot en 3D ---*

# Crear mapa de colores
number = 2             # número de color por partícula
cmap = plt.get_cmap('gnuplot')
colors = [cmap(i) for i in np.linspace(0, 1, number)]
fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
for i, color in enumerate(colors, start=1):
    xini = np.array(rayleigh(xinicial, n, dt, i))
    ax.scatter(xini, yini, zini, color=color)

plt.show()
